const tomScore: { name: string, score: number } = {
    name: 'Tom',
    score: 12
}


interface Dier {
    name: string;
}

interface Hond extends Dier {
    kleur?: string;

    blaf(nummer: number): void;
}

let samson: Hond = {
    blaf: function (num): void {
        console.log("gertjeeeueuuuuhhh", num)
    },
    name: "samson"
}

console.log(samson)
samson.blaf(5)


let x = [null, 1];

type WindowStates = "open" | "closed" | "minimized";

let venster: WindowStates = "closed"


const formElement = document.getElementById("myForm");

// Voeg een event listener toe aan het formulier
formElement!.addEventListener("submit", (event) => {
//         ^ uitroepteken is Non-null assertion operator
    console.log("submet event triggered")
});
